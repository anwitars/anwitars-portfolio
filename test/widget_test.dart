import 'package:flutter_test/flutter_test.dart';

import 'package:anwitars_portfolio/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(const MyApp());

    // There is only a single text in the app.
    expect(find.text('Hello World!'), findsOneWidget);
    expect(find.text('Anything else?'), findsNothing);
  });
}
